import './App.css';
import {MultipleCounter} from "./features/MultipleCounter";

export default function App() {
  return (
    <div className={"App"}>
        <MultipleCounter/>
    </div>
  );
}
import {CounterSizeGenerator} from "./CounterSizeGenerator";
import {CounterGroup} from "./CounterGroup";
import {CounterGroupSum} from "./CounterGroupSum";
import {useSelector} from "react-redux";
import React from "react";

export const MultipleCounter = () => {

    const countList=useSelector((state=>state.counter.counterList));

    const sum = countList.reduce((prev, cur) => prev + cur, 0)

    return (
        <div className={"multipleCounter"}>
            <CounterSizeGenerator />
            <CounterGroupSum sum={sum}/>
            <CounterGroup />
        </div>
    )
}
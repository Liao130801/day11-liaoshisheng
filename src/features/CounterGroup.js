import Counter from "./Counter";
import {useDispatch, useSelector} from "react-redux";
import  {updateCounterValue} from "./counterSlice";
import React from "react";

export const CounterGroup = (props) => {
    const counterList=useSelector((state=>state.counter.counterList));
    const dispatch=useDispatch();
    const handleChange = (value,index) => {
        dispatch(updateCounterValue({value,index}))
    }

    return (
        <div>
            {counterList.map((counter, index) => {
                return (<Counter key={index} counter={counter} updateCounterValue={(value) => handleChange(value,index)}/>)
            })}
        </div>
    )
}
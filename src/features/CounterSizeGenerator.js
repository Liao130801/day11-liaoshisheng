import {updateCounterList} from "./counterSlice"
import {useSelector,useDispatch} from "react-redux";
export const CounterSizeGenerator = (props) => {
    const countList=useSelector((state=>state.counter.counterList));
    const dispatch = useDispatch();
    //const {size,setSize} = props
    const changeSize = (e) => {
        //setSize(e.target.value)
        dispatch(updateCounterList(e.target.value))
    }

    return (
        <div className={"counterSizeGenerator"}>
            size:
            <input type="number" value={countList.length || ''} onChange={changeSize}/>
        </div>
    )
}
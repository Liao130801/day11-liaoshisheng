import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        counterList: [1,3]
    },
    reducers: {
         updateCounterList:(state,action)=>{
             state.counterList = action.payload?Array.from({length: action.payload}).fill(0):[];
         },
         updateCounterValue:(state,action)=>{
             const {value,index} = action.payload;
             state.counterList=state.counterList.map((item,itemIndex) => {
                 return itemIndex===index? value : item;
             })
         },
        updateSum:(state,action)=>{

        }
    }
})

export const {updateCounterList,updateCounterValue} = counterSlice.actions;
export default counterSlice.reducer